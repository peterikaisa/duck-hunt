## Duck-hunt :penguin:
This is a small web app that I used for applying a job at [Vincit](https://www.vincit.fi/). I got the stub for back-end from their [github](https://github.com/Vincit/summer-2018/). Only thing I added was the use of a view engine for rendering my pages. 
### Requirements
Not much is required, mainly [Node.js](https://nodejs.org/) with npm and [Git](https://git-scm.com/), obviously. Please do note that I assume you are running on Linux, I didn't bother changing how the non-default port is determined via command line.
### Installing
If you so wish to have this little project on your machine, here's how to do it:
```
$ git clone https://gitlab.com/peterikaisa/duck-hunt.git
$ cd duck-hunt
$ npm install
```
### Running the app
To start the server type on your terminal with your precious digits:
```
$ npm start
```
or if you prefer some other port than 8081 you may press the keys on your keyboard in the following order where you will type the port you wish to be used instead of `<port>`.
```
$ PORT=<port> node server.js
```
### The Stack
I assume you have the server up and running by now. Let's talk about The Stack! Meaning the frameworks I've used and reasons why I've done so.
##### Node.js, express.js
As the back-end was given this decision wasn't really mine, but I like express so I don't mind. I added a view engine for rendering the initial page, [express-hbs](https://github.com/barc/express-hbs) to be exact. It does what view engines do, you know.
##### Handlebars
"What's Handlebars?", you say. Or at least I imagine you are saying. I know, I'm clearly assuming you haven't heard about it and that's because I know other frameworks are much more popular. [Handlebars](https://handlebarsjs.com/) is a library used to build templates that don't involve logic in themselves but can be populated quite logically. Reasons I chose to use this library were (instead of, say React).
* separate HTML and JavaScript
* easy to populate HTML with data (via tags)
* easy to implement helper methods used to populate HTML, if some logic is needed
* easy to separate HTML in partials to increase modularity and most importantly, code readability

Now, I like jQuery as a library a lot. But I hate it when I have to mix and match my JS with my HTML whenever I want to manipulate my existing HTML without serving static pages. And to be honest, that's basically jQuery. I think it looks like spaghetti bolognese mixed with peasoup. Delicious on their own, disaster together (I'm actually allergic to peasoup but I hear people say it's great). When I went to look what [React](https://reactjs.org/) looks like I thought to myself "Oh dear, I don't want to dive into that". Returning HTML markup that includes JS functions inside the HTML? Damn, that's some impressive parsing right there with all those square brackets. I'm sure it's great and people can do lots of great things with it. Hell, I will probably be developing with React too since it's what the cool kids do these days. I don't complain, I will do what I'm asked but for the sake of doing this app, I went my way. And my way meant that I wanted to populate HTML with data easily and Handlebars allows it via it's tags. My way meant that whenever I knew beforehand how I wanted to display my data I also knew what the HTML would look like and so I wanted to write the template ready to be populated with the actual data and inserted into the page dynamically. No jQuery spaghetti peasoup at all. Now, to be honest, I would compile the script templates beforehand and serve the client with them via AJAX for example but as I was given the server stub I wasn't entirely sure how much I would be allowed to change it. Hell, I even left in the hard coded JSON even though I really wanted to serve them from a file. So now the templates are compiled client side even though I definitely would not want to do that in production. 
##### Bootstrap
You know Bootsrap. It's the most popular component library for front end. I'm pretty sure you've heard about it. I like Bootstrap. Bootstrap is nice. (As a non-graphic designer, it makes my life super easy and makes my stuff look professional even though I suck at everything graphical :heart:)
#### Not my code!
I don't like to invent the wheel all over in some cases. Therefore I've used some components I haven't made myself. These are:
[This](https://tempusdominus.github.io/bootstrap-4/) plugin for picking date and time.
[This](https://useiconic.com/open) icon library used to provide some visuals as I am not a graphic designer. It's really lightweight and the icons look nice. :thumbsup: