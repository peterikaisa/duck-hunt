"use strict";
/**
* In production I would definitely send all html templates via AJAX but I'm not entirely sure how much 
* I'm allowed to tinker with the given server stub and that's why these templates are now compiled client side.
**/
function populatePage(source, target, data) {
	var sourcetmpl = $(source).html();
	var compiled = Handlebars.compile(sourcetmpl);
	$(target).html(compiled(data));
};
function resetForm() {
	$('#sighting-form')[0].reset();
};
function fetchSightings() {
	$.getJSON('/sightings', (data) => {
		var sightings = {};
		sightings['sightings'] = data;
		populatePage('#card-template', '#sighting-cards', sightings);
	});
};
function fetchSpecies() {
	$.getJSON('/species', (data) => {
		var species = {};
		species['species'] = data;
		populatePage('#species-template', '#species-card', species);
	});
};
function postSighting() {
	var validated = validateFormValues();
	if (validated) {
		var species = $('#species-select').val(), 
			rawDate = $('#date').val(),
			description = $('#description').val(),
			count = $('#count').val();

		var dateTime = moment(rawDate, "D.M.Y H:mm").toISOString();
		var json = JSON.stringify( {"id": "0", "species": species, "description": description, "dateTime": dateTime, "count": count} );

		$.ajax({
			url: '/sightings',
			dataType: 'json',
			type: 'post',
			contentType: 'application/json',
			data: json,
			processData: false,
			success: function(result, status, xhr) {
				$('#add-row').collapse('toggle');
				resetForm();
				fetchSightings();
			},
			error: function(xhr, status, error) {
				$('#add-row').collapse('toggle');
				resetForm();
				alert("Something went wrong posting your sighting, sorry!");
			}
		});
	} else {
		alert("Something went wrong posting your sighting, sorry!");
	}
};
/**
* Validates species against api and date to be the right format. I won't bother with white-listing 
* the descriptions and numbers here as it should be done server side anyway.
**/
function validateFormValues() {
	var species = $('#species-select').val(), 
		rawDate = $('#date').val();

	var validSpecies = false;
	$.ajax({
		async: false,
		url: '/species',
		type: 'get',
		dataType: 'json',
		success: function(result) {
			result.forEach((item) => {
				if (item.name === species) {
					validSpecies = true;
				}
			});
		}
	});
	//if species was fine, let's check the date
	if (validSpecies) {
		var m = moment(rawDate, "D.M.Y H:mm");
		return m.isValid();
	}
	return validSpecies;
};
// Sort sightings either ascending or descending. True == ascending.
// Populates the sightings-cards.
function sortSightingsByDateTime(asc) {
	var arr = {};
	arr['sightings'] = [];
	$.getJSON('/sightings', (data) => {
		arr['sightings'] = data.sort( (a,b) => {
			var aDate = a['dateTime'];
			var bDate = b['dateTime'];
			if (moment(aDate).isBefore(bDate)) {
				// aDate < bDate
				return -1;
			} else if (moment(aDate).isSame(bDate)) {
				// aDate === bDate
				return 0;
			} else {
				// aDate > bDate
				return 1;
			}
		});
		if (!asc) {
			arr['sightings'].reverse();
		}
		populatePage('#card-template', '#sighting-cards', arr);
	});
};